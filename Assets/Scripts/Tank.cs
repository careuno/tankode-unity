﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class Tank {
	public string Id;
	public string Name;
	public Color Color;
	public int Health;
	public Vector2 Position;
	public float Rotation;
	public float CannonRotation;
	public TankBehaviour tB;
}


public class JsTank
{
	public string name;
	public int health;
	public float[] position;
	public float rotation;
	public float cannonRotation;

	public JsTank(string name, int health, float[] pos, float rot, float cannonRot) 
	{
		this.name = name;
		this.health = health;
		this.position = pos;
		this.rotation = rot;
		this.cannonRotation = cannonRot;
	}
}
