﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Jurassic;
using Jurassic.Library;
using Newtonsoft.Json;
using System;

public class TankBehaviour : MonoBehaviour
{

	#region public properties
	public Transform Cannon;
	public float Speed = 0;
	public bool IsTurning = false;
	public bool CannonIsTurning = false;
	public GameObject Bullet;
	public GameObject Explosion;
	public Tank tank;
	#endregion

	#region private properties
	private Vector3 moveDirection = Vector3.zero;
	private CharacterController controller;
	private float turningGradees = 0;
	private float settedGradees;
	private float cannonTurningGradees = 0;
	private float cannonSettedGradees;

	private float fieldOfViewAngle = 90f; 
	private float distanceRadar = 50f;

	private bool isRunning = false;
	ScriptEngine engine;

	private float lastShoot;
	#endregion

	void Awake()
	{
		tank.tB = this;
		tank.Health = 100;
		engine  = new ScriptEngine();
		engine.CompatibilityMode = Jurassic.CompatibilityMode.Latest;
		// Engine configuration
		engine.EnableExposedClrTypes = true;
		engine.SetGlobalFunction("message", new System.Action<string>(Messages));

		engine.SetGlobalValue("tank", new TankObject(engine, this));
		engine.SetGlobalValue("time", Time.time);

		engine.Evaluate ("tank.impactReceived = function(enemy) {}; tank.impactSuccess = function(enemy) {}; tank.shootRay = function() { return JSON.parse(tank.shootRaycast); }");
	}

	// Use this for initialization
	void Start () {
		lastShoot = Time.time;
		controller = GetComponent<CharacterController> ();
		controller.detectCollisions = true;	



	//	StartCoroutine (Test());
	}

	void Update () 
	{
		engine.SetGlobalValue("time", Time.time);
		if(isRunning) engine.CallGlobalFunction ("loop");
		// movement
		moveDirection = new Vector3(0, 0, 1);
		moveDirection = transform.TransformDirection(moveDirection);
		moveDirection *= Speed;
		controller.Move(moveDirection * Time.deltaTime);

		// turning tank 
		if (IsTurning) {
			int g;
			if (settedGradees > 0) {
				g = 1;
			} else {
				g = -1;
			}
			transform.Rotate(0, g, 0);
			Debug.Log (transform.eulerAngles.y + " - " + turningGradees);
			if (transform.eulerAngles.y > turningGradees && g == 1 || transform.eulerAngles.y < turningGradees && g == -1) {
				IsTurning = false;
				turningGradees = 0;
				settedGradees = 0;
			} 
		}


		// turning cannon
		if (CannonIsTurning) {
			int g;
			if (cannonSettedGradees > 0) {
				g = 1;
			} else {
				g = -1;
			}
			Cannon.Rotate(0, g, 0);
		
			if (Cannon.eulerAngles.y > cannonTurningGradees && g == 1 || Cannon.eulerAngles.y < cannonTurningGradees && g == -1) {
				CannonIsTurning = false;
				cannonTurningGradees = 0;
				cannonSettedGradees = 0;
			} 
		}


		// update properties
		tank.Position = new Vector2(transform.position.x, transform.position.z);
		tank.Rotation = transform.rotation.y;
		tank.CannonRotation = Cannon.rotation.y;
	}

	public string ShootRay()
	{
		// detect object in front of the tank
		Vector3 fwd = Cannon.TransformDirection(Vector3.forward) * 10;
		RaycastHit hit;

//		Debug.DrawRay(transform.position, fwd, Color.green);

		if (Physics.Raycast (Cannon.position, fwd, out hit, 100.0f)) {
			return "{\"type\": \"" + hit.transform.gameObject.tag + " \", \"name\": \"" + hit.transform.gameObject.name + "\", \"distance\": \"" + hit.distance.ToString () + "\" }";
		} else {
			Debug.Log ("{}");
			return "{}";
		}
	}
		
	public void Turn(float gradees)
	{
		settedGradees = gradees;
		if(gradees != 0)
		{
			IsTurning = true;
			turningGradees = transform.eulerAngles.y + gradees;
			if (turningGradees < 0) {
				turningGradees = 360 + turningGradees;
			}
		}
		else if(IsTurning && gradees == 0)
		{
			IsTurning = false;
		}
	}

	public void TurnCannon(float gradees)
	{
		cannonSettedGradees = gradees;
		if(gradees != 0)
		{
			CannonIsTurning = true;
			cannonTurningGradees = Cannon.eulerAngles.y + gradees;
			if (cannonTurningGradees < 0) {
				cannonTurningGradees = 360 + cannonTurningGradees;
			}
		}
		else if(CannonIsTurning && gradees == 0)
		{
			CannonIsTurning = false;
		}
	}

	public void Shoot()
	{
		var t = Time.time - lastShoot;
		if (t > 2) {
			GameObject b = Instantiate (Bullet);
			Physics.IgnoreCollision (b.GetComponent<Collider> (), GetComponent<Collider> ());
			b.transform.SetParent (Cannon.transform, false);
			b.GetComponent<Bullet> ().Owner = tank;
			b.GetComponent<Rigidbody> ().AddForce (b.transform.forward * 8000);
			Destroy (b, 3f);
			lastShoot = Time.time;
		}
	}

	public void Messages(string message)
	{
		Debug.Log (message);
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Bullet") {
			Tank enemy = collision.gameObject.GetComponent<Bullet> ().Owner;
			Messages ("Disparo recibido de " + enemy.Name);
			enemy.tB.ImpactSuccess (tank);
			GameObject b = (GameObject)Instantiate (Explosion, collision.transform.position, collision.transform.rotation);
			Destroy (b, 1);
			DestroyObject (collision.gameObject);
			string json = JsonFormed (new JsTank (enemy.Name, enemy.Health, new float[] { enemy.Position.x, enemy.Position.y }, enemy.Rotation, enemy.CannonRotation));
			engine.Execute("tank.impactReceived(JSON.parse('" + json + "'));");
		}
	}

	public void ImpactSuccess(Tank victim)
	{
		Messages ("Disparo acertado a " + victim.Name);
		string json = JsonFormed (new JsTank (victim.Name, victim.Health, new float[] { victim.Position.x, victim.Position.y }, victim.Rotation, victim.CannonRotation));
		engine.Execute("tank.impactSuccess(JSON.parse('" + json + "'));");

	}

	private Vector2[] generateTriangleRadar()
	{
		Vector2 p1 = tank.Position;
		float angleP2 = Cannon.eulerAngles.y - (fieldOfViewAngle / 2);
		float angleP3 = Cannon.eulerAngles.y + (fieldOfViewAngle / 2);
		Vector2 p2 = new Vector2(distanceRadar * Mathf.Cos(angleP2), distanceRadar * Mathf.Sin(angleP2));
		Vector3 p3 = new Vector2(distanceRadar * Mathf.Cos(angleP3), distanceRadar * Mathf.Sin(angleP3));
		return new Vector2[] { p1, p2, p3 };
	}

	private float sign(Vector2 p1, Vector2 p2, Vector2 p3)
	{
		return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
	}
		
	bool PointInTriangle(Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3)
	{
		bool b1, b2, b3;

		b1 = sign(pt, v1, v2) < 0;
		b2 = sign(pt, v2, v3) < 0;
		b3 = sign(pt, v3, v1) < 0;

		return ((b1 == b2) && (b2 == b3));
	}

	public List<Tank> DetectedInRadar()
	{
		List<Tank> listTank = new List<Tank> ();
		Vector2[] radarPoints = generateTriangleRadar ();
		var tanksObject = GameObject.FindGameObjectsWithTag ("Tank");
		/*
		GameObject.Find ("p1").transform.position = new Vector3 (radarPoints[0].x, 0, radarPoints[0].y);
		GameObject.Find ("p2").transform.position = new Vector3 (radarPoints[1].x, 0, radarPoints[1].y);
		GameObject.Find ("p3").transform.position = new Vector3 (radarPoints[2].x, 0, radarPoints[2].y);
*/
		foreach(GameObject t in tanksObject)
		{
			var tankTemp = t.GetComponent<TankBehaviour> ().tank;
			Vector2 pt = tankTemp.Position;
			if(PointInTriangle(pt, radarPoints[0], radarPoints[1], radarPoints[2]))
			{
				listTank.Add (tankTemp);
			}

		}
		return listTank;
	}

	private string JsonFormed(JsTank t, bool isArray = false)
	{
		string json = JsonConvert.SerializeObject(t);
		return json;
	}
		

	IEnumerator Test() 
	{
		yield return new WaitForSeconds (1f);
		//Shoot();
		//TurnCannon(90);
		//Debug.Log(ShootRay());
		engine.Execute ("tank.impactReceived = function(enemy) { message(enemy.name)}; tank.setSpeed(0); tank.shoot();");
	}


	public void Run(string code)
	{
		
		try
		{			
			engine.Execute(code);
			isRunning = true;
		}catch(Exception e)
		{
			Debug.Log (e.Message);
		}
	}
}
	
public class TankObject : ObjectInstance
{
	TankBehaviour tankCore;
	public TankObject(ScriptEngine engine, TankBehaviour core) : base(engine)
	{
		tankCore = core;
		this.PopulateFunctions();
		this.PopulateFields ();
	}

	[JSFunction(Name = "setSpeed")]
	public void SetSpeed(double velocity)
	{
		if (velocity > 20)
			velocity = 20;
		if (velocity < -20)
			velocity = -20;
		tankCore.Speed = (float)velocity;
	}

	[JSFunction(Name = "getSpeed")]
	public double GetSpeed()
	{
		return (double)tankCore.Speed;
	}

	[JSFunction(Name = "isTurning")]
	public bool IsTurning()
	{
		return tankCore.IsTurning;
	}

	[JSFunction(Name = "cannonIsTurning")]
	public bool CannonIsTurning()
	{
		return tankCore.CannonIsTurning;
	}

	[JSFunction(Name = "turn")]
	public void Turn(double gradees)
	{
		tankCore.Turn ((float)gradees);
	}

	[JSFunction(Name = "turnCannon")]
	public void TurnCannon(double gradees)
	{
		tankCore.TurnCannon ((float)gradees);
	}

	[JSFunction(Name = "shoot")]
	public void Shoot()
	{
		tankCore.Shoot ();
	}

	[JSFunction(Name = "shootRaycast")]
	public string ShootRay()
	{
		return tankCore.ShootRay ();
	}
}

/*
 * 
 * Ajustar Axes cannon
 * 
 * 
 * */