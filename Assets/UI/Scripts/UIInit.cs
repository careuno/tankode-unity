﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System;
using UnityEngine.SceneManagement;

public class UIInit : MonoBehaviour {
	public GameObject DialogCreate;

	public void ClickCreateNewTank()
	{
		DialogCreate.SetActive (true);
	}

	public void ClickAccept()
	{
		string path = EditorUtility.OpenFilePanel("Load your JS file", "~/Desktop", "js"); 
		try
		{   // Open the text file using a stream reader.
			using (StreamReader sr = new StreamReader(path))
			{
				// Read the stream to a string, and write the string to the console.
				String line = sr.ReadToEnd();
				//Debug.Log(line);
				PlayerPrefs.SetString("codeUser", line);
				PlayerPrefs.SetString("name", "Carlos");

	
				PlayerPrefs.SetString("id", "wererqpwf903241r");
				SceneManager.LoadScene("main");
			}
		}
		catch (Exception e)
		{
			Debug.Log("The file could not be read:");
			Debug.Log(e.Message);
		}
	}


	
}
