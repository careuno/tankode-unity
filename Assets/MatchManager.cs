﻿using UnityEngine;
using System.Collections;

public class MatchManager : MonoBehaviour {
	public GameObject Player;
	// Use this for initialization
	void Start () {
		GameObject pl = Instantiate (Player);
		pl.GetComponent<TankBehaviour> ().tank.Name = PlayerPrefs.GetString ("name");
		pl.GetComponent<TankBehaviour> ().tank.Id = PlayerPrefs.GetString ("id");

		GameObject pl2 = Instantiate (Player);
		pl2.transform.position = new Vector3 (55,0,55);
		pl2.transform.rotation = Quaternion.Euler (new Vector3(0, 180, 0));
		pl2.GetComponent<TankBehaviour> ().tank.Name = "CPU";

		pl2.GetComponent<TankBehaviour> ().Run("function loop() {}");
		pl.GetComponent<TankBehaviour> ().Run(PlayerPrefs.GetString("codeUser"));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
